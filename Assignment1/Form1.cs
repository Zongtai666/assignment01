﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double S, K, t, r, vol;
            S = Convert.ToDouble(textBox1.Text);
            K = Convert.ToDouble(textBox2.Text);
            r = Convert.ToDouble(textBox3.Text);
            t = Convert.ToDouble(textBox4.Text);
            vol = Convert.ToDouble(textBox5.Text);
            int Nsim;
            int Nstep;
            Nsim = Convert.ToInt32(textBox6.Text);
            Nstep = Convert.ToInt32(textBox7.Text);
            double[,]c= Program.BoxMuller(Nsim, Nstep - 1);
            if (checkBox1.Checked == false)
            {
                textBox8.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox9.Text = Convert.ToString(Program.CallDelta(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox10.Text = Convert.ToString(Program.CallGamma(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox11.Text = Convert.ToString(Program.CallVega(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox12.Text = Convert.ToString(Program.CallTheta(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox13.Text = Convert.ToString(Program.CallRho(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox14.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[1]);

                textBox15.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[2]);
                textBox16.Text = Convert.ToString(Program.PutDelta(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox17.Text = Convert.ToString(Program.PutGamma(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox18.Text = Convert.ToString(Program.PutVega(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox19.Text = Convert.ToString(Program.PutTheta(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox20.Text = Convert.ToString(Program.PutRho(Nsim, Nstep, S, K, r, t, vol, c)[0]);
                textBox21.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[3]);
            }

            if (checkBox1.Checked == true)
            {
                textBox8.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[4]);
                textBox9.Text = Convert.ToString(Program.CallDelta(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox10.Text = Convert.ToString(Program.CallGamma(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox11.Text = Convert.ToString(Program.CallVega(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox12.Text = Convert.ToString(Program.CallTheta(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox13.Text = Convert.ToString(Program.CallRho(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox14.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[5]);

                textBox15.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[6]);
                textBox16.Text = Convert.ToString(Program.PutDelta(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox17.Text = Convert.ToString(Program.PutGamma(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox18.Text = Convert.ToString(Program.PutVega(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox19.Text = Convert.ToString(Program.PutTheta(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox20.Text = Convert.ToString(Program.PutRho(Nsim, Nstep, S, K, r, t, vol, c)[1]);
                textBox21.Text = Convert.ToString(Program.Price(Nsim, Nstep, S, K, r, t, vol, c)[7]);
            }
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

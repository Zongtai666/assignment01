﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment1
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }



        public static double ran1 = 0, ran2 = 0;

        public static double[,] BoxMuller(int n, int t)
        {
            double[,] rand = new double[n, t];
            double w, c;
            Random rnd = new Random();
            for (int i = 0; i < n; i = i + 2)
            {
                for (int j = 0; j < t; j++)
                {
                    do
                    {
                        ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                        ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                        w = ran1 * ran1 + ran2 * ran2;
                    }
                    while (w > 1);
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    rand[i, j] = c * ran1;
                    rand[i + 1, j] = c * ran2;
                }
            }
            return rand;

        }









        public static double[] Price(int Nsim, int Nstep, double S, double K, double r, double T, double sigma, double[,] c)
        {

            double[,] price = new double[Nsim, Nstep];
            double[,] price1 = new double[Nsim, Nstep];
            int i, j;
            double St, St1;
            double t = T / (Nstep - 1);
            for (i = 1; i <= Nsim; i++)
            {
                St = S;
                St1 = S;
                price[i - 1, 0] = St;
                price1[i - 1, 0] = St1;
                for (j = 1; j < Nstep; j++)
                {
                    St = St * Math.Exp((r - 0.5 * sigma * sigma) * t + sigma * Math.Sqrt(t) * c[i - 1, j - 1]);
                    price[i - 1, j] = St;
                    St1 = St1 * Math.Exp((r - 0.5 * sigma * sigma) * t - sigma * Math.Sqrt(t) * c[i - 1, j - 1]);
                    price1[i - 1, j] = St1;
                }

            }
            double cp = 0;
            double cp1 = 0;
            double pp = 0;
            double pp1 = 0;
            for (i = 0; i < Nsim; i++)
            {
                cp = cp + Math.Max(price[i, Nstep - 1] - K, 0);
                cp1 = cp1 + Math.Max(price[i, Nstep - 1] - K, 0) + Math.Max(price1[i, Nstep - 1] - K, 0);
                pp = pp + Math.Max(K - price1[i, Nstep - 1], 0);
                pp1 = pp1 + Math.Max(K - price[i, Nstep - 1], 0) + Math.Max(K - price1[i, Nstep - 1], 0);
            }

            cp = cp / Nsim * Math.Exp(-1 * r * T);
            cp1 = cp1 /2/ Nsim * Math.Exp(-1 * r * T);
            pp = pp / Nsim * Math.Exp(-1 * r * T);
            pp1 = pp1 /2/Nsim * Math.Exp(-1 * r * T);
            double SE1 = 0;
            double SE2 = 0;
            double SE3 = 0;
            double SE4 = 0;
            for (i = 0; i < Nsim; i++)
            {
                SE1 = SE1 + (cp - Math.Max(price[i, Nstep - 1] - K, 0)) * (cp - Math.Max(price[i, Nstep - 1] - K, 0));
                SE2 = SE2 + (pp - Math.Max(K - price[i, Nstep - 1], 0)) * (pp - Math.Max(K - price[i, Nstep - 1], 0));
                SE3 = SE3 + (cp1 - (Math.Max(price[i, Nstep - 1] - K, 0) + Math.Max(price1[i, Nstep - 1] - K, 0)) / 2)* (cp1 - (Math.Max(price[i, Nstep - 1] - K, 0) + Math.Max(price1[i, Nstep - 1] - K, 0)) / 2);
                SE4 = SE4 + (pp1 - (Math.Max(K - price[i, Nstep - 1], 0) + Math.Max(K - price1[i, Nstep - 1], 0)) / 2)* (pp1 - (Math.Max(K - price[i, Nstep - 1], 0) + Math.Max(K - price1[i, Nstep - 1], 0)) / 2);
            }
            SE1 = Math.Sqrt(SE1 / (Nsim - 1) / (Nsim)) * Math.Exp(-r * T);
            SE2 = Math.Sqrt(SE2 / (Nsim - 1) / (Nsim)) * Math.Exp(-r * T);
            SE3 = Math.Sqrt(SE3 / (Nsim - 1) / (Nsim)) * Math.Exp(-r * T);
            SE4 = Math.Sqrt(SE4 / (Nsim - 1) / (Nsim)) * Math.Exp(-r * T);
            double[] results = new double[8];
            results[0] = cp;
            results[1] = SE1;
            results[2] = pp;
            results[3] = SE2;
            results[4] = cp1;
            results[5] = SE3;
            results[6] = pp1;
            results[7] = SE4;
            return results;
        }










        public static double[] CallDelta(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] calldelta = new double[2];
            calldelta[0] = (Price(n1, n2, 1.001 * S, K, r, t, vol, c)[0] - Price(n1, n2, 0.999 * S, K, r, t, vol, c)[0]) / (0.002 * S);
            calldelta[1] = (Price(n1, n2, 1.001 * S, K, r, t, vol, c)[4] - Price(n1, n2, 0.999 * S, K, r, t, vol, c)[4]) / (0.002 * S);
            return calldelta;
        }
        public static double[] PutDelta(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] putdelta = new double[2];
            putdelta[0] = (Price(n1, n2, 1.001 * S, K, r, t, vol, c)[2] - Price(n1, n2, 0.999 * S, K, r, t, vol, c)[2]) / (0.002 * S);
            putdelta[1] = (Price(n1, n2, 1.001 * S, K, r, t, vol, c)[6] - Price(n1, n2, 0.999 * S, K, r, t, vol, c)[6]) / (0.002 * S);
            return putdelta;
        }
        public static double[] CallGamma(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] callgamma = new double[2];
            double dS = 0.001 * S;
            callgamma[0] = (Price(n1, n2, S + dS, K, r, t, vol, c)[0] - 2.0 * Price(n1, n2, S, K, r, t, vol, c)[0] + Price(n1, n2, S - dS, K, r, t, vol, c)[0]) / Math.Pow(dS, 2);
            callgamma[1] = (Price(n1, n2, S + dS, K, r, t, vol, c)[4] - 2.0 * Price(n1, n2, S, K, r, t, vol, c)[4] + Price(n1, n2, S - dS, K, r, t, vol, c)[4]) / Math.Pow(dS, 2);
            return callgamma;
        }
        public static double[] PutGamma(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] putgamma = new double[2];
            double dS = 0.001 * S;
            putgamma[0] = (Price(n1, n2, S + dS, K, r, t, vol, c)[2] - 2.0 * Price(n1, n2, S, K, r, t, vol, c)[2] + Price(n1, n2, S - dS, K, r, t, vol, c)[2]) / Math.Pow(dS, 2);
            putgamma[1] = (Price(n1, n2, S + dS, K, r, t, vol, c)[6] - 2.0 * Price(n1, n2, S, K, r, t, vol, c)[6] + Price(n1, n2, S - dS, K, r, t, vol, c)[6]) / Math.Pow(dS, 2);
            return putgamma;
        }
        public static double[] CallVega(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] callvega = new double[2];
            double dvol = 0.001 * vol;
            callvega[0] = (Price(n1, n2, S, K, r, t, vol + dvol, c)[0] - Price(n1, n2, S, K, r, t, vol - dvol, c)[0]) / 2.0 / dvol;
            callvega[1] = (Price(n1, n2, S, K, r, t, vol + dvol, c)[4] - Price(n1, n2, S, K, r, t, vol - dvol, c)[4]) / 2.0 / dvol;
            return callvega;
        }
        public static double[] PutVega(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] putvega = new double[2];
            double dvol = 0.001 * vol;
            putvega[0] = (Price(n1, n2, S, K, r, t, vol + dvol, c)[2] - Price(n1, n2, S, K, r, t, vol - dvol, c)[2]) / 2.0 / dvol;
            putvega[1] = (Price(n1, n2, S, K, r, t, vol + dvol, c)[6] - Price(n1, n2, S, K, r, t, vol - dvol, c)[6]) / 2.0 / dvol;
            return putvega;
        }
        public static double[] CallTheta(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] calltheta = new double[2];
            double dt = 0.001 * t;
            calltheta[0] = (Price(n1, n2, S, K, r, t + dt, vol, c)[0] - Price(n1, n2, S, K, r, t, vol, c)[0]) / dt;
            calltheta[1] = (Price(n1, n2, S, K, r, t + dt, vol, c)[4] - Price(n1, n2, S, K, r, t, vol, c)[4]) / dt;
            return calltheta;
        }
        public static double[] PutTheta(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] puttheta = new double[2];
            double dt = 0.001 * t;
            puttheta[0] = (Price(n1, n2, S, K, r, t + dt, vol, c)[2] - Price(n1, n2, S, K, r, t, vol, c)[2]) / dt;
            puttheta[1] = (Price(n1, n2, S, K, r, t + dt, vol, c)[6] - Price(n1, n2, S, K, r, t, vol, c)[6]) / dt;
            return puttheta;
        }
        public static double[] CallRho(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] callrho = new double[2];
            double dr = 0.001 * r;
            callrho[0] = (Price(n1, n2, S, K, r + dr, t, vol, c)[0] - Price(n1, n2, S, K, r - dr, t, vol, c)[0]) / 2.0 / dr;
            callrho[1] = (Price(n1, n2, S, K, r + dr, t, vol, c)[4] - Price(n1, n2, S, K, r - dr, t, vol, c)[4]) / 2.0 / dr;
            return callrho;
        }
        public static double[] PutRho(int n1, int n2, double S, double K, double r, double t, double vol, double[,] c)
        {
            double[] putrho = new double[2];
            double dr = 0.001 * r;
            putrho[0] = (Price(n1, n2, S, K, r + dr, t, vol, c)[2] - Price(n1, n2, S, K, r - dr, t, vol, c)[2]) / 2.0 / dr;
            putrho[1] = (Price(n1, n2, S, K, r + dr, t, vol, c)[6] - Price(n1, n2, S, K, r - dr, t, vol, c)[6]) / 2.0 / dr;
            return putrho;
        }
    }
}
